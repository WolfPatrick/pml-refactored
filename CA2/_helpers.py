import _params as _p
import random


def read_file(delimiter=',', decimal_separator='.'):
    with open(_p.data_file, 'r') as data:
        _p.feature_names = [feature.replace('\n', '') for feature in data.readline().split(delimiter)]
        return [line.replace(decimal_separator, '.').replace(delimiter, ',').replace('\n', '').split(',') for line in
                data.readlines()]


def filter_data(data, indices):
    return [data[x] for x in range(len(data)) if x not in indices]


def split_features_targets(data):
    features = []
    targets = []
    for line in data:
        features.append([float(x) for x in line[:len(line) - 1]])
        targets.append(line[len(line) - 1])

    return features, targets


def merge_features_targets(features, targets):
    data = []
    for i in range(len(features)):
        data.append([features[i] + [targets[i]]][0])

    return data


def add_custom_feature(features):
    for custom_feature in _p.custom_features:
        _p.features.append(_p.features[len(_p.features) - 1] + 1)
        _p.feature_names.append(custom_feature.replace('feature', ''))
        for feature in features:
            feature.append(eval(custom_feature))

    return features


def filter_features(features, feature_filter=_p.features, override_feature_names=True):
    if override_feature_names:
        _p.feature_names = [_p.feature_names[i] for i in feature_filter]
    return [[f[i] for i in feature_filter] for f in features]


def split_training_test(data, split):
    training_data = []
    test_data = []

    for x in data:
        if random.random() <= split:
            training_data.append(x)
        else:
            test_data.append(x)

    return [training_data, test_data]


def print_line(line, line_break=True):
    with open(f"{_p.result_dir}results.md", 'a') as file:
        if line_break:
            file.write(f"{line}\n")
        else:
            file.write(f"{line}")
        file.close()
