import os

base_dir = os.path.abspath(__file__ + "/../")
result_dir = f'{base_dir}/result/'
data_file = f'{base_dir}/data/Iris0100.csv'
feature_names = []
features = [2, 3]
custom_features = ['feature[0]/feature[1]']
lines_to_ignore = "range(len(data))[::2]"
training_size = 0.5

knn = {
    "k": 5
}

rf = {
    "jobs": 2,
    "random_state": 0,
    "estimators": 50
}

svm = {
    "kernel": "linear"
}