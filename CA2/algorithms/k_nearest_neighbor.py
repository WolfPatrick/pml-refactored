import _params as _p
import _helpers as _h
import itertools as it
from sklearn.neighbors import KNeighborsClassifier


def preprocessing(data, count=1, filter_bad_values=True):
    features, targets = _h.split_features_targets(data)

    if count <= _p.knn["k"]:
        outliers = []
        knn = KNeighborsClassifier(n_neighbors=count)
        knn_model = knn.fit(features, targets)
        nearest_nb = knn_model.kneighbors(n_neighbors=count, return_distance=False)
        for x in range(len(nearest_nb)):
            if targets[x] != targets[nearest_nb[x][count - 1]]:
                outliers.append(x)

        _h.print_line(f"- k = {count}: `{outliers}`")

        if filter_bad_values:
            return preprocessing(_h.filter_data(data, outliers), count + 1, filter_bad_values)
        else:
            return preprocessing(data, count + 1, filter_bad_values)

    return data


def get_combinations(data):
    features, targets = _h.split_features_targets(data)
    all_combinations = []
    for i in range(1, len(_p.feature_names) + 1):
        for x in list(it.combinations(_p.feature_names, i)):
            all_combinations.append(list(x))

    for combination in all_combinations:
        filtered_features = _h.filter_features(features, [_p.feature_names.index(x) for x in combination], False)
        # print()
        training_data, test_data = _h.split_training_test(_h.merge_features_targets(filtered_features, targets), _p.training_size)
        training_features, training_targets = _h.split_features_targets(training_data)
        test_features, test_targets = _h.split_features_targets(test_data)
        knn = KNeighborsClassifier(n_neighbors=_p.knn["k"])
        knn.fit(training_features, training_targets)
        _h.print_line(f'{combination} | {knn.score(test_features, test_targets):1.2f}')
        # print([(x, _p.feature_names.index(x)) for x in combination])
    # exit()


def predict(d, training_features, training_targets):
    knn = KNeighborsClassifier(n_neighbors=_p.knn["k"])
    knn.fit(training_features, training_targets)
    return knn.predict([d[:-1]])[0]
