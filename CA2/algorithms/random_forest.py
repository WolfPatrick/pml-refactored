import _params as _p
from sklearn.ensemble import RandomForestClassifier


def predict(d, training_features, training_targets):
    rf = RandomForestClassifier(n_jobs=_p.rf["jobs"], random_state=_p.rf["random_state"],
                                n_estimators=_p.rf["estimators"])
    rf.fit(training_features, training_targets)
    return rf.predict([d[:-1]])[0]
