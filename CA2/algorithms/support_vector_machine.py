import _params as _p
from sklearn.svm import SVC as svc


def predict(d, training_features, training_targets):
    svm = svc(kernel=_p.svm["kernel"])
    svm.fit(training_features, training_targets)
    return svm.predict([d[:-1]])[0]
