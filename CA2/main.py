import os
import sys
import _params as _p
import _helpers as _h
import collections
import time

from algorithms import k_nearest_neighbor as _knn
from algorithms import random_forest as _rf
from algorithms import support_vector_machine as _svm


def clean_up():
    try:
        for file in os.listdir(_p.result_dir):
            os.remove(_p.result_dir + file)
    except FileNotFoundError:
        os.makedirs(_p.result_dir)


def prepare_data(data):
    filtered_data = _h.filter_data(data, eval(_p.lines_to_ignore))
    features, targets = _h.split_features_targets(filtered_data)
    features = _h.filter_features(features)
    features = _h.add_custom_feature(features)
    return _h.merge_features_targets(features, targets)


def main():
    clean_up()
    data = _h.read_file(delimiter=';', decimal_separator=',')
    original_data = data
    filtered_data = _h.filter_data(original_data, eval(_p.lines_to_ignore))
    fails = {
        'knn': 0,
        'rf': 0,
        'svm': 0,
        'ensemble': 0
    }

    data = prepare_data(data)

    _h.print_line('# K Max Predict & Preprocessing')
    _h.print_line('## removing outliers disabled')
    _knn.preprocessing(data=data, filter_bad_values=False)
    _h.print_line('## removing outliers enabled')

    data = _knn.preprocessing(data)

    _h.print_line(f'## feature combinations\ncombinations | accuracy\n--- | ---')

    _knn.get_combinations(filtered_data)

    _h.print_line(f'## preprocessing result'
                  f'\nlength before filtering: {len(original_data)}'
                  f'\n\nlength after filtering: {len(filtered_data)}'
                  f'\n\nlength after removing outliers: {len(data)}')

    _h.print_line(f'# Ensemble Learning')
    for fn in _p.feature_names:
        _h.print_line(f'{fn} | ', False)
    _h.print_line('knn | random forest | svm | ensemble learning | actual')
    _h.print_line('--- | ' * (len(_p.feature_names) + 4) + '---')

    for i in range(len(data)):
        sys.stdout.write(f'\r[{"#" * int(i/len(data) * 20)}{" " * int((1 - i/len(data)) * 20)}]'
                         f'\tprocessing data set {i + 1} of {len(data)}...')
        d = data[i]
        for row in d[:len(d) - 1]:
            _h.print_line(f'{row:.2f} | ', False)

        training_features, training_targets = _h.split_features_targets(_h.filter_data(data, [i]))
        knn_prediction = _knn.predict(d, training_features, training_targets)
        rf_prediction = _rf.predict(d, training_features, training_targets)
        svm_prediction = _svm.predict(d, training_features, training_targets)

        ensemble_prediction = sorted([knn_prediction, rf_prediction, svm_prediction],
                                     key=collections.Counter([knn_prediction, rf_prediction, svm_prediction]).get,
                                     reverse=True)[0]
        actual_target = d[len(d) - 1]

        for prediction in ["knn", "rf", "svm", "ensemble"]:
            if eval(prediction + "_prediction") != actual_target:
                fails[prediction] += 1
                _h.print_line(f'***{eval(prediction + "_prediction")}*** | ', False)
            else:
                _h.print_line(f'{eval(prediction + "_prediction")} | ', False)
        _h.print_line(f'{actual_target}')

    _h.print_line(f'# Accuracy')
    for prediction in ["knn", "rf", "svm", "ensemble"]:
        _h.print_line(f'- {prediction}: {(1 - fails[prediction] / len(data)) * 100:.2f} %')

    sys.stdout.write(f'\rProcess finished in {time.process_time():.2f} s. '
                     f'\nResults can be found in {_p.result_dir.replace("/", os.sep)}')


main()
