# K Max Predict & Preprocessing
## removing outliers disabled
- k = 1: `[1, 7, 13, 61]`
- k = 2: `[1, 18]`
- k = 3: `[1, 7, 13, 24, 37, 45]`
- k = 4: `[1, 7, 11, 13, 31]`
- k = 5: `[1, 7, 11, 27, 38, 65]`
## removing outliers enabled
- k = 1: `[1, 7, 13, 61]`
- k = 2: `[]`
- k = 3: `[9]`
- k = 4: `[]`
- k = 5: `[60]`
## feature combinations
combinations | accuracy
--- | ---
['petal length'] | 0.69
['petal width'] | 0.40
['[0]/[1]'] | 0.94
['petal length', 'petal width'] | 0.78
['petal length', '[0]/[1]'] | 0.88
['petal width', '[0]/[1]'] | 0.90
['petal length', 'petal width', '[0]/[1]'] | 0.84
## preprocessing result
length before filtering: 150

length after filtering: 75

length after removing outliers: 69
# Ensemble Learning
petal length | petal width | [0]/[1] | knn | random forest | svm | ensemble learning | actual
--- | --- | --- | --- | --- | --- | --- | ---
4.00 | 1.00 | 4.00 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
4.00 | 1.30 | 3.08 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
3.30 | 1.00 | 3.30 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
3.80 | 1.10 | 3.45 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
3.90 | 1.10 | 3.55 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
4.00 | 1.30 | 3.08 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
5.00 | 2.00 | 2.50 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
3.50 | 1.00 | 3.50 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
4.00 | 1.20 | 3.33 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
4.10 | 1.00 | 4.10 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
5.10 | 1.90 | 2.68 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
4.90 | 1.80 | 2.72 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
4.60 | 1.50 | 3.07 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
4.00 | 1.30 | 3.08 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
4.80 | 1.40 | 3.43 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
5.10 | 2.40 | 2.12 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
6.70 | 2.00 | 3.35 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
5.60 | 2.10 | 2.67 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
5.60 | 2.20 | 2.55 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
1.40 | 0.20 | 7.00 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
4.70 | 1.40 | 3.36 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
4.30 | 1.30 | 3.31 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
4.20 | 1.30 | 3.23 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
5.60 | 1.80 | 3.11 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
1.40 | 0.20 | 7.00 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.10 | 0.10 | 11.00 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.30 | 0.20 | 6.50 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
4.20 | 1.50 | 2.80 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
4.40 | 1.40 | 3.14 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
4.50 | 1.50 | 3.00 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
4.60 | 1.40 | 3.29 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
5.90 | 2.10 | 2.81 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
6.60 | 2.10 | 3.14 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
5.50 | 1.80 | 3.06 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
5.80 | 1.60 | 3.62 | Iris-virginica | ***Iris-versicolor*** | Iris-virginica | Iris-virginica | Iris-virginica
4.80 | 1.80 | 2.67 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
5.20 | 2.00 | 2.60 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
1.50 | 0.20 | 7.50 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.60 | 0.20 | 8.00 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.50 | 0.10 | 15.00 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
4.40 | 1.40 | 3.14 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
5.50 | 1.80 | 3.06 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
5.60 | 2.40 | 2.33 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
1.30 | 0.20 | 6.50 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.20 | 0.20 | 6.00 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.40 | 0.20 | 7.00 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
4.50 | 1.50 | 3.00 | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor | Iris-versicolor
5.10 | 2.00 | 2.55 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
5.70 | 2.30 | 2.48 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
5.90 | 2.30 | 2.57 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
1.40 | 0.20 | 7.00 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
6.00 | 2.50 | 2.40 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
5.70 | 2.50 | 2.28 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
1.50 | 0.20 | 7.50 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.70 | 0.20 | 8.50 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.60 | 0.40 | 4.00 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.50 | 0.40 | 3.75 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
5.40 | 2.30 | 2.35 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
1.40 | 0.30 | 4.67 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.30 | 0.20 | 6.50 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.00 | 0.20 | 5.00 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.50 | 0.20 | 7.50 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.50 | 0.20 | 7.50 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.50 | 0.30 | 5.00 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.60 | 0.20 | 8.00 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
6.40 | 2.00 | 3.20 | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica | Iris-virginica
1.30 | 0.40 | 3.25 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.50 | 0.10 | 15.00 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
1.50 | 0.40 | 3.75 | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa | Iris-setosa
# Accuracy
- knn: 100.00 %
- rf: 98.55 %
- svm: 100.00 %
- ensemble: 100.00 %
