import _params as _p
import re

wordtable = {}
spam_mail_count = 0
ham_mail_count = 0


def split_mail(mail):
    return {
        'header': mail.split('\n\n')[0],
        'body': '\n\n'.join(mail.split('\n\n')[1:])
    }


def whitelist(mail):
    for address in _p.whitelist:
        if address in mail['header']:
            return 'whitelist', 1
    return 'undetermined', -1


def blacklist(mail):
    for address in _p.blacklist:
        if address in mail['header']:
            return 'blacklist', 1
    return 'undetermined', -1


def naive_bayes(mail):
    probabilities = []
    unknown_words = []
    for word in re.split('\W+', mail['body']):
        if len(word) > 0:
            try:
                wt = wordtable[word.lower()]
                likelihood_spam = wt['spam_messages'] / spam_mail_count
                likelihood_ham = wt['ham_messages'] / ham_mail_count
                probabilities.append(float(likelihood_spam / (likelihood_ham + likelihood_spam)))
                # für jedes wort:
                # spam mails mit dem wort / alle spam mails = likelihood für spam
                # ham mails mit dem wort / alle ham mails = likelihood für ham
                # spam prob / ( spam prob + ham prob ) = wahrscheinlichkeit durch normalisierung
            except KeyError:
                unknown_words.append(word.lower())

    if len(unknown_words) > 0:
        print_log('\t##### Unknown words that could not be rated:')
        [print_log(f'\t- {word}') for word in unknown_words]

    prob = sum(probabilities) / len(probabilities)

    if prob < _p.ham_treshold:
        return 'ham', prob
    if prob > _p.spam_treshold:
        return 'spam', prob
    return 'undetermined', prob


def print_word_table(file, line):
    file.write(f'{line}\n')


def print_result(line):
    with open(_p.result_file, 'a', encoding=_p.encoding) as file:
        file.write(f'{line}\n')
        file.close()


def print_log(line):
    with open(_p.log_file, 'a', encoding=_p.encoding) as file:
        file.write(f'{line}\n\n')
        file.close()
