import os

base_dir = os.path.abspath(__file__ + "/../")
result_dir = f'{base_dir}/dir.filter.results/'
output_dir = f'{base_dir}/dir.mail.output/'
input_dir = f'{base_dir}/dir.mail.input/'
spam_dir = f'{base_dir}/dir.spam/'
ham_dir = f'{base_dir}/dir.nospam'
word_table = f'{result_dir}nb.wordtable.md'
result_file = f'{result_dir}spamfilter.results.md'
log_file = f'{result_dir}spamfilter.log.md'
priority_order = ['naive_bayes', 'blacklist', 'whitelist']
blacklist = [x.replace("\n", "") for x in open(os.path.join(base_dir, 'blacklist'), "r").readlines()]
whitelist = [x.replace("\n", "") for x in open(os.path.join(base_dir, 'whitelist'), "r").readlines()]
ham_treshold = 0.5
spam_treshold = 0.8
encoding = 'latin1'
