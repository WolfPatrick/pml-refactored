	Von:	Frederik Dietrich <Frederik.Dietrich@Spiratec-AG.com>
	Gesendet:	Mittwoch, 21. März 2018 15:34
	An:	r.gerten@hs-mannheim.de
	Betreff:	Kooperationsmöglichkeiten im Bereich Industrial IT zwischen HS Mannheim und SpiraTec AG
	XSpam:	Ham
	XSpamProbability:	0.18552702755121322
####
	Von:	Im Auftrag der Bibliothek der HS Mannheim <service24@ibs-bw.de>
	Gesendet:	Dienstag, 10. April 2018 14:50
	An:	r.gerten@hs-mannheim.de
	Betreff:	Bestellung in der Bibliothek für: Gerten, Rainer
	XSpam:	Ham
	XSpamProbability:	0.1731733704995737
####
	Von:	E-3 Magazin <web@b4bmedia.net>
	Gesendet:	Donnerstag, 28. Juni 2018 14:17
	An:	r.gerten@hs-mannheim.de
	Betreff:	E-3 Newsletter
	XSpam:	Spam
	XSpamProbability:	0.8888273685654408
####
	Von:	Jake Carpenter <uclaeditorjake3@gmail.com>
	Gesendet:	Samstag, 7. Juli 2018 04:04
	An:	r.gerten@hs-mannheim.de
	Betreff:	Hello Professor Gerten. I am UCLA journal manuscript editor, Jake Carpenter.
	XSpam:	Spam
	XSpamProbability:	0.8615725310969472
####
	Von:	Professoren <professoren-bounces@ml.hs-mannheim.de> im Auftrag von Oliver Hummel <o.hummel@hs-mannheim.de>
	Gesendet:	Freitag, 23. November 2018 12:29
	An:	professoren@hs-mannheim.de; professoren-ruhestand@hs-mannheim.de; mitarbeiter@hs-mannheim.de
	Betreff:	[Professoren] Informatik-Kolloquium: das Internet of Things naht unaufhaltsam
	Anlagen:	Abstract 27-11-18.pdf
	XSpam:	Ham
	XSpamProbability:	0.29668695987728594
####
	Von:	Testsender <sender@test.de>
	Gesendet:	Dienstag, 10.12.2018 14:00
	An:	r.gerten@hs-mannheim.de
	Betreff:	Testmail zur Bestimmung des undetermined Bereichs
	XSpam:	Spam
	XSpamProbability:	0.8483990717377801
####
