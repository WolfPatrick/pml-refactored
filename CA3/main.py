import os
import re
import _params as _p
import _helpers as _h


def clean_up():
    for folder in [_p.output_dir, _p.result_dir]:
        try:
            for file in os.listdir(folder):
                os.remove(folder + file)
        except FileNotFoundError:
            os.makedirs(folder)


def count_words(words, spam):
        word = words[0]
        count = words.count(word)
        try:
            check = _h.wordtable[word][spam + '_words']
        except KeyError:
            _h.wordtable[word] = {
                'spam_words': 0,
                'spam_messages': 0,
                'ham_words': 0,
                'ham_messages': 0
            }
        finally:
            _h.wordtable[word][spam + '_words'] += count
            _h.wordtable[word][spam + '_messages'] += 1

        words = [x for x in words if x != word]
        if len(words) > 0:
            count_words(words, spam)


def create_wordtable():

    _h.print_log('## Spam Directory')
    for mail in os.listdir(_p.spam_dir):
        _h.print_log(f'- {mail}')
        _h.spam_mail_count += 1
        with open(f'{_p.spam_dir}/{mail}', 'r', encoding=_p.encoding) as mail:
            words = [x.lower() for x in re.split('\W+', _h.split_mail(mail.read())['body']) if len(x) > 0]
            count_words(words, 'spam')

    _h.print_log('## Ham Directory')
    for mail in os.listdir(_p.ham_dir):
        _h.print_log(f'- {mail}')
        _h.ham_mail_count += 1
        with open(f'{_p.ham_dir}/{mail}', 'r', encoding=_p.encoding) as mail:
            words = [x.lower() for x in re.split('\W+', _h.split_mail(mail.read())['body']) if len(x) > 0]
            count_words(words, 'ham')

    with open(_p.word_table, 'a') as file:
        file.write('word | spam mails | spam words | ham mails | ham words | mail prob. | word prob.\n'
                            '--- | --- | --- | --- | --- | --- | ---\n')
        for key, value in _h.wordtable.items():
            file.write(f'{key} | {value["spam_messages"]} | {value["spam_words"]} | {value["ham_messages"]} | '
                  f'{value["ham_words"]} | {value["spam_messages"] / (value["spam_messages"] + value["ham_messages"]):1.3f} | '
                  f'{value["spam_words"] / (value["spam_words"] + value["ham_words"]):1.3f}\n')

        file.close()
    _h.print_log('## Word Table')
    wt_path = '../' + os.path.relpath(_p.word_table).replace(os.path.dirname(_p.word_table), "./").replace("\\", "/")
    _h.print_log(f'{len(_h.wordtable)} unique words have been rated.\n\n'
                 f'Results can be found in [{wt_path}]({wt_path})')


def rate_mail(mail):
    is_spam = 'undetermined'
    spam_probability = 0

    for algorithm in _p.priority_order:
        if is_spam == 'undetermined':
            result = eval(f'_h.{algorithm}({mail})')
            is_spam = result[0]
            if result[1] > -1:
                spam_probability = result[1]

    return is_spam, spam_probability


def main():
    clean_up()

    create_wordtable()

    _h.print_log('## Input Directory')
    for file in os.listdir(_p.input_dir):
        _h.print_log(f'- {file}')
        with open(f'{_p.input_dir}/{file}', 'r', encoding=_p.encoding) as mail:
            text = _h.split_mail(mail.read())
            rating = rate_mail(text)
            text["header"] += f'\nXSpam:\t{rating[0].capitalize()}\nXSpamProbability:\t{rating[1]}'
            _h.print_result('\t' + '\n\t'.join(text["header"].split('\n')) + '\n####')
            with open(f'{_p.output_dir}/{file}', 'w', encoding=_p.encoding) as outputMail:
                outputMail.write(f'{text["header"]}\n\n{text["body"]}')
                outputMail.close()

    print(len(os.listdir(_p.input_dir)), 'mails have been processed.')


main()
