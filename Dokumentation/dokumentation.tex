\documentclass[12pt, a4]{article}
\usepackage[ngerman]{babel}
\usepackage{layout}
\usepackage[utf8]{inputenc}
\usepackage{graphicx} 
\usepackage{makeidx}
\usepackage{biblatex}
\usepackage{csquotes}
\usepackage{hyperref}[linkcolor = black,
plainpages = false,
hypertexnames = false,
citecolor = black]
\usepackage{float}
\usepackage{listings}
\makeindex

\begin{document}
\begin{titlepage}
    \begin{center}
        \vspace{1cm}
        \large
        \textbf{Dokumentation zu CA2 und CA3}
        \vspace{0.3cm}
        \\06.01.2019
        \vspace{1cm}
        \\Veranstaltung:
        \\
        \textbf{Partikuläre Datenanalyse\\und Maschinelles Lernen}
        \vspace{1cm}
        \\
        \textbf{Patrick Wolf} 
        \\Matrikelnummer: 1429439
        \\Unternehmens- und Wirtschaftsinformatik 
        \\Hochschule Mannheim
    
    \end{center}
\end{titlepage}


\pagenumbering{Roman}

% Inhaltsverzeichnis
\tableofcontents

\newpage
\pagenumbering{arabic}

\section{Einleitung}
In dieser Dokumentation wird die Struktur, Vorgehensweise und die jeweiligen Ergebnisse des praktischen Leistungsnachweises für die Veranstaltung PML beschrieben.\\
Bei beiden Projekten handelt es sich um Python-Anwendungen. Die verwendete IDE\footnote{Integrierte Entwicklungsumgebung} war PyCharm\footnote{\href{https://www.jetbrains.com/pycharm/}{https://www.jetbrains.com/pycharm/}}.
Die Projektstruktur sieht für beide im Folgenden beschriebenen CA-Teile Unterordner vor. Auch diese Dokumentation liegt in einem Unterordner. Alles zusammen befindet sich in einem git\footnote{Open Source Software zur Versionsverwaltung}-Repository zur einfachen Versionsverwaltung. Das Repository ist bei GitLab\footnote{\href{https://gitlab.com/WolfPatrick/pml-refactored}{https://gitlab.com/WolfPatrick/pml-refactored}} gehostet und öffentlich zugänglich.\\
In den Unterordnern der beiden Software-Projekte finden sich neben weiteren projektspezifischen Dateien und Ordnern jeweils eine \emph{main.py}, die die auszuführende Python-Datei ist. Zudem gibt es eine \emph{requirements.txt}, die die benötigten Libraries auflistet. Beide Dateien werden in der \emph{README.md} nochmals erläutert.\\
Außerdem beinhalten die beiden Ordner jeweils eine \emph{\_params.py}, in der Parameter definiert werden, die schnell zu ändern sein sollen, sowie eine \emph{\_helpers.py}, in der häufig verwendete Hilfsfunktionen wie das Schreiben in eine Ergebnisdatei definiert sind.\\
Die Ergebnisdateien werden im Markdown\footnote{\href{https://daringfireball.net/projects/markdown/}{https://daringfireball.net/projects/markdown/}}-Format .md erzeugt, da viele Ausgaben in Tabellenform erfolgen und Markdown eine sehr simple Formatierung erlaubt. Markdown-Dateien können in vielen Texteditoren editiert und auch angeschaut werden. Auch bietet GitLab eine Vorschau für solche Dateien, sie können also auch online über das Repository angeschaut werden. Markdown ist eine sehr simple Auszeichnungssprache und der Standard für Readme-Dateien in git-Repositories.\\
In \ref{sec:Abbildungen} finden sich beispielhaft einige Ausgaben zur Veranschaulichung.\\
Die Ergebnis-Ordner werden jeweils in einer Funktion \emph{clean\_up} zu Anfang geleert. Dies wird mit Python gelöst, um auf Bash-Skripte verzichten zu können. Dadurch kann das Programm unabhängig vom Betriebssystem genutzt werden.

\section{CA2: Ensemble Learning}

\subsection{Architektur}
\includegraphics[width=0.5\textwidth]{img/struktur_ca2.jpg}
\\Im Projekt Ensemble Learning gibt es drei Unterordner. Im Ordner \emph{algorithms} finden sich die Python-Files für die drei verwendeten Algorithmen.\\
Im Ordner \emph{data} befinden sich die Daten-Dateien zur Analyse. Der Dateiname der zu analysierenden Datei wird als Parameter in der Parameter-Datei angegeben.\\
Im \emph{result}-Ordner befindet sich die Ergebnis-Datei. Auf diese wird in \ref{ca2:ergebnis} weiter eingegangen.

\subsection{Eingangsparameter}
Die Parameter-Datei enthält die Pfade zu den Eingangsdaten sowie zur Ergebnis-Datei, die zu betrachtenden Features sowie die sog. \emph{custom features}, also selbst definierte, berechnete Features. Außerdem können die zu ignorierenden Datensätze angegeben werden, was dann per \emph{eval}-Funktion interpretiert wird. Zusätzlich finden sich einige Parameter für die verschiedenen Algorithmen, darunter das Verhältnis zwischen Trainingsdaten und Testdaten für die \emph{knn}-Berechnung für \emph{k max predict}, und einige spezifische Parameter wie bspw. die Anzahl der Jobs für den \emph{random forest}, das \emph{k} für \emph{knn} oder den kernel für die \emph{support vector machine}.

\subsection{Funktionalität und Ablauf}
Zu Beginn wird die vorige Ergebnis-Datei gelöscht oder initial der \emph{result}-Ordner erstellt. Danach werden die Input-Daten ausgelesen und aufbereitet. Hierzu werden die in den Parametern eingestellten zu ignorierenden Zeilen ausgeschlossen, in diesem Fall jede zweite Zeile. Danach werden die übrigen Datensätze in ihre sog. \emph{features} und \emph{targets} aufgeteilt. Das bedeutet, dass die einzelnen Features, wie im Iris Data Set\footnote{frei verfügbare Daten zu Blumen aus dem Jahre 1936, \\\href{https://en.wikipedia.org/wiki/Iris\_flower\_data\_set}{https://en.wikipedia.org/wiki/Iris\_flower\_data\_set}} die Breite und Länge der Blüten und Blätter in eine Liste gespeichert werden. Die Targets, beim Iris Data Set dann die Blumenart, werden in einer zweiten Liste gespeichert. Danach werden gemäß der definierten Parameter die Features gefiltert sowie eigene Features hinzugefügt. Danach werden die beiden Listen wieder vereinigt.\\
Anschließend beginnt die Vorselektion der Daten, indem durch \emph{k max predict} Ausreißer entfernt werden. Bei diesem Klassifizierungsalgorithmus werden alle Datensätze entfernt, deren nächste Nachbarn einer anderen Kategorie, also hier Blumenart, zugeordnet sind. Das wird rekursiv so oft ausgeführt, wie es in der Parameter-Datei definiert ist, beispielsweise fünf mal. Dabei wird die Anzahl der zu betrachtenden Nachbarn jeweils um eins erhöht.\\
Dieser \emph{k max predict} wird zwei mal durchgeführt. Ein mal werden die Ausreißer sofort entfernt, bevor die Anzahl der Nachbarn erhöht wird, und ein mal werden die Ausreißer nicht entfernt.\\
Im nächsten Schritt werden alle Feature-Kombinationen mit dem \emph{knn}-Algorithmus auf ihre Vorhersage-Wahrscheinlichkeit getestet. Dazu werden die Datensätze in Trainings- und Test-Daten aufgeteilt. Die Trainings-Daten werden zum Lernen für den Algorithmus verwendet, die Test-Daten werden vorhergesagt und die Genauigkeit bzw. Qualität der Vorhersagen wird ermittelt.\\
Danach wird mit den voreingestellten Features für alle Datensätze die Vorhersage-Wahrscheinlichkeit ermittelt, indem alle anderen Datensätze als Trainings-Daten benutzt werden und das Programm mit einer Kombination aus den drei verschiedenen Algorithmen \emph{knn}, \emph{random forest} und \emph{support vector machine} lernt. Nach diesem Ensemble Learning wird die Kategorie des Test-Datensatzes mit allen drei Algorithmen vorhergesagt und mit seiner wahren Kategorie abgeglichen. Es werden die in den Parametern definierten Features verwendet, statt der Feature-Kombination mit der höchsten Präzision, da die Verbesserung der Qualität nicht zwingend die unter Umständen wesentlich schlechtere Performance rechtfertigt.\\
Da alle Datensätze einzeln getestet werden, jeweils mit allen anderen Daten, muss sehr viel mit den Daten gearbeitet werden und die Ausführung des Programms kann einige Sekunden dauern. Aus diesem Grund wurde eine Fortschrittsanzeige programmiert, wie der folgende Screenshot zeigt.\\
\includegraphics[width=1.0\textwidth]{img/fortschritt_ca2.png}
Nachdem die Qualität des Ensemble Learnings beendet ist und die Ausgabedatei fertiggestellt ist, wird die Ausgabe verworfen und durch die im folgenden Screenshot zu sehende Ausgabe ersetzt.\\
\includegraphics[width=1.0\textwidth]{img/fortschritt100_ca2.png}


\subsection{Ergebnis}\label{ca2:ergebnis}
Die Ergebnis-Datei enthält alle Ergebnisse des \emph{k max predict} und alle verschiedenen Feature-Kombinationen mit ihrer Vorhersage-Genauigkeit.\\
Zusätzlich sind die Ergebnisse der Vorselektion enthalten.\\
Natürlich werden auch die Ergebnisse aus dem Ensemble Learning dargestellt. Hierzu werden alle Datensätze mit ihren vorhergesagten und tatsächlichen Werten in tabellarischer Form aufgelistet und zum Schluss die Genauigkeit der einzelnen Algorithmen sowie des Ensemble Learning berechnet.\\
In Abbildung \ref{fig:anlage_1} findet sich eine beispielhafte Ergebnis-Datei, bei der die Tabelle stark gekürzt ist.

\newpage

\section{CA3: Bayes-Filter}

\subsection{Architektur}
\includegraphics[width=0.5\textwidth]{img/struktur_ca3.jpg}
\\Im Projekt Bayes-Filter ist die Struktur durch ein Template weitestgehend vorgegeben worden. Diese Struktur wurde größtenteils übernommen. Es befinden sich fünf Unterordner im Projekt, und zwar jeweils ein Ordner für Spam- und Nicht-Spam-Mails (im Folgenden auch Ham-Mails genannt), der Posteingang sowie zwei Ergebnis-Ordner. Im einen befinden sich die Mails aus dem Posteingang nach der Bewertung, im anderen liegt die Ergebnis-Übersicht, die Worttabelle sowie ein Log-File. Diese Dateien sind der Formatierung wegen wieder Markdown-Files.\\
Zusätzlich gibt es eine whitelist und eine blacklist, in denen jeweils Mail-Adressen oder Domains hinterlegt sind. Außerdem gibt es gemäß der Vorlage eine run\_spamfilter.py, die lediglich dazu dient, die main-Funktion aufzurufen.

\subsection{Einangsparameter}
Die Parameter-Datei enthält ähnlich zu CA2 alle Ordner- und Dateipfade. Außerdem findet sich die sog. \emph{priority order}, also die Reihenfolge der Algorithmen zur Bewertung einer Mail im Posteingang. \\
Zudem werden hier die blacklist sowie die whitelist ausgelesen und in Variablen gespeichert. Alternativ könnte man die Variablen auch mit Listen belegen, weshalb sie in der Parameter-Datei definiert sind.\\
Zusätzlich sind die Grenzwerte für die Bewertung mit dem \emph{naive bayes}-Algorithmus definiert, also bis zu welchem Wert eine Mail als Ham gilt und ab welchem Wert sie als Spam gekennzeichnet wird.\\
Zu guter Letzt wurde das Format zur Encodierung von Dateien als Parameter definiert, um einfach auf Formatänderungen reagieren zu können und es nicht an mehreren Stellen hart eincodieren zu müssen.

\subsection{Funktionalität und Ablauf}
Zu Beginn werden analog zu CA2 die Output- bzw. Ergebnis-Ordner bereinigt. \\
Im nächsten Schritt wird die bereits erwähnte Worttabelle erstellt. Hierzu werden alle Mails aus dem Spam- und Ham-Verzeichnis analysiert. Hierzu wird zuerst ein leeres Dictionary angelegt. Danach wird in jeder Mail jedes Wort mit den Häufigkeiten von null dem Dictionary hinzugefügt, falls es noch nicht abgelegt ist. \\
Das Dictionary sieht mit dem ersten Eintrag also wie folgt aus:

\newpage
\lstset{language=Python}

\begin{lstlisting}[frame=single]
{
  'hello': {
    'spam_words': 0,
    'spam_messages': 0,
    'ham_words': 0,
    'ham_messages': 0	
  }
}
\end{lstlisting}

Das Wort stellt sich hier also als Key eines geschachtelten Dictionaries dar. Die Werte sind \emph{spam\_words}, in welchem die absolute Häufigkeit des Wortes in Spam-Mails gespeichert wird, sowie \emph{spam\_messages}, wo die Anzahl der Spam-Mails gespeichert wird, in denen das Wort vorkommt. Analog verhält es sich mit den beiden übrigen Werten im Bezug auf Ham-Mails. Diese Werte werden später zur Berechnung der Spam-Wahrscheinlichkeit mit dem \emph{naive bayes}-Algorithmus benötigt.\\
Falls das Wort schon in dem Dictionary der Worttabelle vorhanden ist, werden die Werte entsprechend erhöht. \\
Wird also das Wort \emph{hello} beispielsweise in vier verschiedenen Spam-Mails gefunden und tritt insgesamt elf mal in diesen vier Spam-Mails auf, wird das resultierende Dictionary wie folgt aussehen:

\begin{lstlisting}[frame=single]
{
  'hello': {
    'spam_words': 11,
    'spam_messages': 4,
    'ham_words': 0,
    'ham_messages': 0	
  }
}
\end{lstlisting}

Nachdem alle Worte in allen Mails entsprechend im Dictionary hinterlegt sind, können die Mails aus dem Input-Ordner bewertet werden. Dies geschieht gemäß der bereits erwähnten \emph{priority order}. Sobald ein Algorithmus ein Ergebnis zurückliefert, ist diese Mail bewertet. Sollte ein Algorithmus kein Ergebnis zurückliefern können, so wird der nächste ausgeführt.\\
Hier wird beispielsweise nur auf Black- und Whitelist geprüft, wenn der \emph{naive bayes} kein eindeutiges Ergebnis liefern kann.\\
Das Prüfen auf Black- bzw. Whitelist ist hier sehr simpel gehalten, es wird nur für alle Elemente in der jeweiligen Liste geprüft, ob sie im Header der Mail vorkommen. Der Header wird hier definiert als der Teil der Mail bis zum ersten doppelten Zeilenumbruch.\\
Der \emph{naive bayes} hingegen ist ein etwas komplexerer Algorithmus, bei dem für jedes Wort die sog. \emph{likelihood} für Spam bzw. Ham berechnet wird. Die \emph{likelihood} für Spam berechnet sich aus dem Verhältnis von Spam-Mails, die das Wort beinhalten, zu allen Spam-Mails. Analog wird die \emph{likelihood} für Ham berechnet. Durch Normalisierung kann nun die Wahrscheinlichkeit für Spam berechnet werden. Da das für alle Worte gemacht werden muss, wird der Durchschnitt aller Wahrscheinlichkeiten als die Spam-Wahrscheinlichkeit gewertet. Die Formel zum \emph{naive bayes} stammt aus einem Skript der FH Köln\footnote{\href{http://www.gm.fh-koeln.de/~konen/WPF-DM-Cup/04-Naive-Bayes.pdf}{http://www.gm.fh-koeln.de/~konen/WPF-DM-Cup/04-Naive-Bayes.pdf}} und kann daher abweichende Ergebnisse produzieren.\\
Sollte eine Mail ein Wort enthalten, das noch nicht im Dictionary hinterlegt ist, wird es in diesem Fall aus der Bewertung ausgeschlossen. 

\subsection{Ergebnis}
Jede bewertete Mail wird mit zwei zusätzlichen Header-Feldern, XSpam und XSpamProbability, in den Output-Ordner geschrieben. Der Name der Mail bleibt dabei erhalten. XSpam beinhaltet das Ergebnis der Bewertung, XSpamProbability beinhaltet die Vorhersagewahrscheinlichkeit des \emph{naive bayes} (vgl. Abbildung \ref{fig:anlage_2}). \\
Zudem gibt es mehrere Ergebnis-Dateien im dir.filter.results-Ordner. Sie sind analog zu den anderen Ergebnis-Dateien im Markdown-Format.\\
Zum einen findet sich hier die Worttabelle, die aus dem Dictionary generiert wird. Ein kleiner Ausschnitt davon findet sich in Abbildung \ref{fig:anlage_3}.\\
Ebenso befindet sich hier ein Log-File, das die Inhalte des Spam- und Ham-Ordners auflistet, die Anzahl der Wörter in der verlinkten Worttabelle darstellt und den Posteingang auflistet. Hier werden bei den einzelnen Mails zusätzlich die Wörter angezeigt, die nicht in die Bewertung mit aufgenommen wurden, weil sie in der Worttabelle noch fehlten. Eine gekürzte Version zur Veranschaulichung findet sich in Abbildung \ref{fig:anlage_4}.\\
Außerdem gibt es eine Ergebnis-Datei, in der alle bewerteten Mails nochmal als Überblick dargestellt werden. Eine beispielhafte Datei findet sich in Abbildung \ref{fig:anlage_5}.

\newpage
\section{Abbildungen}\label{sec:Abbildungen}

\newpage
\pagestyle{empty}
\begin{figure}
\vspace*{-3.5cm}
\hspace{-3cm}
\includegraphics[height=24.7cm]{img/ergebnis_ca2.jpg}
\caption{Ergebnis Ensemble Learning\label{fig:anlage_1}}
\end{figure}

\newpage
\begin{figure}
\hspace{-3cm}
\includegraphics[width=19.5cm]{img/output_ca3.jpg}
\caption{Header einer bewerteten Mail\label{fig:anlage_2}}
\end{figure}

\begin{figure}
\hspace{-3cm}
\includegraphics[width=19.5cm]{img/wordtable_ca3.jpg}
\caption{Auszug aus der Worttabelle\label{fig:anlage_3}}
\end{figure}

\newpage
\begin{figure}
\vspace*{-3.5cm}
\hspace{-3cm}
\includegraphics[height=24.7cm]{img/logfile_ca3.jpg}
\caption{Auszug aus dem Log File\label{fig:anlage_4}}
\end{figure}

\newpage
\begin{figure}
%\vspace*{-3.5cm}
\hspace{-3cm}
\includegraphics[width=19.5cm]{img/ergebnis_ca3.jpg}
\caption{Ergebnis Bayes-Filter\label{fig:anlage_5}}
\end{figure}

\end{document}